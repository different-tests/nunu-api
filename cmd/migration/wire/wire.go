//go:build wireinject
// +build wireinject

package wire

import (
	"nunu-api/cmd/migration/internal"
	"nunu-api/internal/repository"
	"nunu-api/pkg/log"
	"github.com/google/wire"
	"github.com/spf13/viper"
)

var RepositorySet = wire.NewSet(
	repository.NewDB,
	repository.NewRedis,
	repository.NewRepository,
	repository.NewUserRepository,
)

func NewApp(*viper.Viper, *log.Logger) (*internal.Migrate, func(), error) {
	panic(wire.Build(
		RepositorySet,
		internal.NewMigrate,
	))
}
