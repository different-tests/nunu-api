package main

import (
	"nunu-api/cmd/migration/wire"
	"nunu-api/pkg/config"
	"nunu-api/pkg/log"
)

func main() {
	conf := config.NewConfig()
	logger := log.NewLog(conf)

	app, cleanup, err := wire.NewApp(conf, logger)
	if err != nil {
		panic(err)
	}
	app.Run()
	defer cleanup()
}
