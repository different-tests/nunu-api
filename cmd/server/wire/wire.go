//go:build wireinject
// +build wireinject

package wire

import (
	"nunu-api/internal/handler"
	"nunu-api/internal/repository"
	"nunu-api/internal/server"
	"nunu-api/internal/service"
	"nunu-api/pkg/helper/sid"
	"nunu-api/pkg/jwt"
	"nunu-api/pkg/log"
	"github.com/google/wire"
	"github.com/spf13/viper"
)

var HandlerSet = wire.NewSet(
	handler.NewHandler,
	handler.NewUserHandler,
)

var ServiceSet = wire.NewSet(
	service.NewService,
	service.NewUserService,
)

var RepositorySet = wire.NewSet(
	repository.NewDB,
	repository.NewRedis,
	repository.NewRepository,
	repository.NewUserRepository,
)

func NewApp(*viper.Viper, *log.Logger) (*server.Server, func(), error) {
	panic(wire.Build(
		RepositorySet,
		ServiceSet,
		HandlerSet,
		server.NewServer,
		server.NewServerHTTP,
		sid.NewSid,
		jwt.NewJwt,
	))
}
